package net.unit8.examples;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class QRGeneratorTest {
    ApplicationContext context;

    @BeforeEach
    void setup() {
        App app = new App();
        context = app.createContext();
    }
    @Test
    void test() {
        UserQRService userQRService = context.getBean(UserQRService.class);
        for(long i=0; i<10; i++) {
            User user = new User(i, UUID.randomUUID());
            userQRService.generate(user);
        }
    }
}