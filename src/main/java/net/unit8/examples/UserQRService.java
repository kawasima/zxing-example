package net.unit8.examples;

import com.google.zxing.WriterException;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;

public class UserQRService {
    private final QRGenerator generator;
    private final File directory;

    public UserQRService(QRGenerator generator) {
        this.generator = generator;
        this.directory = new File("qrcode");
    }

    public void generate(User user) {
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                throw new IllegalStateException("Failed to create a directory " + directory);
            }
        }
        try {
            File file = new File(directory, user.getId() + ".png");
            generator.generateToFile(user.getCode().toString(), file);
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        } catch (WriterException e) {
            throw new RuntimeException(e);
        }
    }
}
