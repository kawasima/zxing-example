package net.unit8.examples;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;

public class QRGenerator {
    final BarcodeFormat format = BarcodeFormat.QR_CODE;
    int width = 200;
    int height = 200;
    String imageType = "png";

    Map<EncodeHintType, Object> hints = Map.of(
            EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M
    );

    public void generateToFile(String contents, File outputFile) throws WriterException, IOException {
        Map<EncodeHintType, ?> hints = Map.of(
                EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M
        );
        QRCodeWriter writer = new QRCodeWriter();
        BitMatrix bitMatrix = writer.encode(contents, format, width, height, hints);
        BufferedImage image = MatrixToImageWriter.toBufferedImage(bitMatrix);
        ImageIO.write(image, "png", outputFile);
    }

    public void setHeight(int height) {
        assert height > 0;
        this.height = height;
    }

    public void setWidth(int width) {
        assert width > 0;
        this.width = width;
    }

    public void setImageType(String type) {
        this.imageType = type;
    }

    public void setOption(EncodeHintType hintType, Object value) {
        hints.put(hintType, value);
    }
}