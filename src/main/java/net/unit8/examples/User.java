package net.unit8.examples;

import lombok.Value;

import java.io.Serializable;
import java.util.UUID;

@Value
public class User implements Serializable {
    Long id;
    UUID code;
}
