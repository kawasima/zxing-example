package net.unit8.examples;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.StaticApplicationContext;

public class App {
    public ApplicationContext createContext() {
        StaticApplicationContext context = new StaticApplicationContext();
        context.registerBean(QRGenerator.class);
        context.registerBean(UserQRService.class);
        return context;
    }
}
